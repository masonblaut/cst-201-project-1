// Mason Blaut
// This is my own work

#include <iostream>
#include "MyString.h"
using namespace std;

int main()
{
	MyString *string1 = new MyString;
	
	//testing initializer
	cout << "String capacity = " << string1->getCapacity() << "\n";
	cout << "String length = " << string1->getLength() << "\n";

	MyString* string2 = new MyString("abcdefg");
	cout << "string2 = " << string2->toString() << "\n";
	cout << "\n";

	MyString* string3 = new MyString("1234567");
	cout << "string3 = " << string3->toString() << "\n";
	cout << "\n";

	//testing Concat
	cout << "Concat Test -- " << "\n";
	MyString* string4 = string2->concat(string3);
	cout << "string2 and string3 = " << string4->toString() << "\n";

	//testing Equal
	cout << "Equals Test -- " << "\n";
	MyString* string5 = new MyString("1234567");
	cout << "string5 = " << string5->toString() << "\n";
	if (string3->equals(string5))
	{
		cout << "string 3 equals string 5!!" << "\n";
	}
	else
	{
		cout << "string 3 does not equal string 5!!" << "\n";
	}
	cout << "\n";

	//Compare To Test
	cout << "Compare To Test -- " << "\n";
	MyString* string6 = new MyString("abc");
	cout << "string6 = " << string6->toString() << "\n";
	MyString* string7 = new MyString("efg");
	cout << "string7 = " << string7->toString() << "\n";
	cout << "Comparing -" << "\n";
	int compNum = string6->compareTo(string7);
	if (compNum == 0)
	{
		cout << "The two are exactly the same!" << "\n";
	}
	else if (compNum == -1)
	{
		cout << "string 6 is first" << "\n";
	}
	else if (compNum == 1)
	{
		cout << "string 7 is first" << "\n";
	}
	cout << "\n";

	//Get test
	cout << "Get Test -- " << "\n";
	cout << string2->get(2) << " is the character at index 2 in string 2- " << string2->toString() << "\n";
	cout << "\n";

	//ToUpper test
	cout << "ToUpper Test -- " << "\n";
	cout << "before = " << string6->toString() << "\n";
	string6->toUpper();
	cout << "after = " << string6->toString() << "\n";
	cout << "\n";

	//ToLower Test
	cout << "ToLower Test -- " << "\n";
	cout << "before = " << string6->toString() << "\n";
	string6->toLower();
	cout << "after = " << string6->toString() << "\n";
	cout << "\n";

	//SubString Test
	cout << "SubString Test -- " << "\n";
	cout << string2->toString() << "\n";
	MyString* subString2 = string2->subString(5);
	cout << subString2->toString() << "\n";
	cout << "\n";
    

	//SubString (n to m) test
	cout << "SubString n to m Test -- " << "\n";
	cout << string2->toString() << "\n";
	MyString* subString2NM = string2->subString(2, 6);
	cout << subString2NM->toString() << "\n";
	cout << "\n";

	//index of Test
	cout << "IndexOf Test -- " << "\n";
	cout << string2->toString() << "\n";
	MyString* stringToFind = new MyString("cde");
	cout << "String to find = " << stringToFind->toString() << "\n";
	cout << "The string is located at index " << string2->indexOf(stringToFind) << "\n";
	cout << "\n";

	//Last Index of Test
	cout << "LastIndexOf Test -- " << "\n";
	MyString* string10 = new MyString("abd123abc123");
	cout << "String10 = " << string10->toString() << "\n";
	MyString* stringToFind2 = new MyString("123");
	cout << "String to find = " << stringToFind2->toString() << "\n";
	cout << "The last index this sting occurs is at " << string10->lastIndexOf(stringToFind2) << "\n";
	cout << "\n";

	cout << "\n" << "END\n";
}
