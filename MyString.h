//Mason Blaut
//This is my own work

#pragma once
#include <string>
using namespace std;

class MyString
{
private:
	char* theString;
	int curr_length; // number of characters currently stored in the string
	int capacity; // how large the string is
public:

	MyString();  // default constructor
	MyString(string s); // initialize the char array

	int getLength(); 
	int getCapacity();
	void ensureCapacity();
	string toString(); // returns string containing the contents of the char array
	MyString* concat(MyString*); 
	bool equals(MyString*);
	int compareTo(MyString*); // returns 0 if they return the same, -1
	char get(int); //return char at index
	MyString* toUpper(); //returns an all upper case version of what is there
	MyString* toLower(); // returns an all lower case version
	MyString* subString(int); // returns a myString that starts at the index given
	MyString* subString(int n, int m); //returns a substring between n and m
	int indexOf(MyString*); //Returns the index where the string found occurs. Returns -1 if not found.
	int lastIndexOf(MyString*); //Finds the last possible index where the string occurs and returns that value. Returns -1 if not found.

};

