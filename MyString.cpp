//Mason Blaut
//This is my own work

#include "MyString.h"
#include <iostream> 
#include <string.h> 

using namespace std;

MyString::MyString()
{
	this->curr_length = 0;
	this->capacity = 1;
	theString = new char[capacity];
}

MyString::MyString(string s)
{
	this->curr_length = 0;
	this->capacity = 1;
	theString = new char[capacity];
	char* temp;

	for (int i = 0; i < s.length(); i++)
	{
		theString[i] = s.at(i);
		this->curr_length++;
		if (curr_length == capacity)
		{
			temp = theString;
			capacity = capacity*2;
			theString = new char[capacity];
			for (int j = 0; j < curr_length; j++)
			{
				theString[j] = temp[j];
			}
			
			delete temp;
		}
		
	}

}

int MyString::getLength()
{
	return this->curr_length;
}

int MyString::getCapacity()
{
	return this->capacity;
}

void MyString::ensureCapacity()
{
}

string MyString::toString()
{
	//cout << "toString started ";
	string s = "";
	for (int i = 0; i < curr_length; i++)
	{
		char c = this->theString[i];
		s.push_back(c);
	}
	return s;
}

MyString* MyString::concat(MyString* str)
{
	MyString* newArray = this;
	char* temp;
	for (int i = 0; i < str->getLength(); i++)
	{
		if (newArray->getLength() == newArray->getCapacity())
		{
			temp = newArray->theString;
			int lng = newArray->getLength();
			newArray->capacity = newArray->getCapacity() * 2;
			newArray->theString = new char[newArray->getCapacity()];
			for (int j = 0; j < lng; j++)
			{
				newArray->theString[j] = temp[j];
			}
		}
		newArray->theString[curr_length] = str->theString[i];
		newArray->curr_length++;
	}
	return newArray;
}

bool MyString::equals(MyString* str)
{
	if (str->getLength() != this->curr_length)
	{
		return false;
	}
	else
	{
		int sharedLength = this->curr_length;
		for (int i = 0; i < sharedLength; i++)
		{
			if (theString[i] == str->theString[i])
			{ }
			else
			{
				return false;
				break;
			}
		}
		return true;
	}

}

int MyString::compareTo(MyString* str)
{
	if (this->equals(str))
	{
		return 0;
	}
	else if (theString[0] > str->theString[0])
	{
		return 1;
	}
	else if (theString[0] < str->theString[0])
	{
		return -1;
	}
	else
	{
		return -1;
	}

}

char MyString::get(int i)
{
	return theString[i];
}

MyString* MyString::toUpper()
{
	for (int i = 0; i < curr_length; i++)
	{
		char temp = theString[i];
		theString[i] = toupper(temp);
	}

	return this;
}

MyString* MyString::toLower()
{
	for (int i = 0; i < curr_length; i++)
	{
		char temp = theString[i];
		theString[i] = tolower(temp);
	}

	return this;
}

MyString* MyString::subString(int num)
{
	
	if (num > curr_length)
	{
		return nullptr;
	}
	else
	{
		string s = "";
		for (int i = num; i < curr_length; i++)
		{
			
			char c = this->theString[i];
			s.push_back(c);
		}
		MyString* temp = new MyString(s);
		return temp;
	}
}

MyString* MyString::subString(int n, int m)
{
	if (n > curr_length || n > curr_length)
	{
		return nullptr;
	}
	else
	{
		string s = "";
		for (int i = n; i < m; i++)
		{

			char c = this->theString[i];
			s.push_back(c);
		}
		MyString* temp = new MyString(s);
		return temp;
	}
}

int MyString::indexOf(MyString* str)
{
	int lengthToFind = str->curr_length;
	for (int i = 0; i < curr_length; i++)
	{
		if (theString[i] == str->theString[0])
		{
			int startSpot = i;
			int counter = 0;
			for (int j = 0; j < str->getLength(); j++)
			{
				if (theString[i + j] == str->theString[j])
				{
					counter++;
				}
			}

			if (counter == str->getLength())
			{
				return i;
			}
		}
	}
	return -1;
}

int MyString::lastIndexOf(MyString* str)
{
	int lengthToFind = str->curr_length;
	int lastIndex = 0;
	for (int i = 0; i < curr_length; i++)
	{
		if (theString[i] == str->theString[0])
		{
			int startSpot = i;
			int counter = 0;
			for (int j = 0; j < str->getLength(); j++)
			{
				if (theString[i + j] == str->theString[j])
				{
					counter++;
				}
			}

			if (counter == str->getLength())
			{
				lastIndex = i;
			}
		}
	}
	if (lastIndex != 0)
	{
		return lastIndex;
	}
	else
	{
		return -1;
	}
	///////   return -1;
}

